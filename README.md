# About
Colibri is a Twitter clone we created as a project assignment for the modern C++ course. 
It offers its users features such as adding posts, interacting with them(likes,dislikes,retweets), adding friends etc. <br/>
Through this project we learned to: <br/>
-develop a client server application using modern C++ features <br/>
-create a GUI using SFML <br/>
-socket programming using Winsock

# Members <br/>
-Ailincai Andrei <br/>
-Barariu Claudiu <br/>
-Boicu Razvan <br/>
-Cimpianu Adrian 

# Project components <br/>
-we created the widgets we needed for the project(buttons, textboxes etc) using SFML <br/>
-the server is able to handle multiple clients at once, we implemented it using a threadpool and every new connection is assigned to a worker thread